package com.gibran.goosegame.game;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import com.gibran.goosegame.dice.Dice;

public class GooseGame {

	private static final int MINIMUM_PLAYERS = 2;
	private static final Integer THE_BRIDGE_JUMP_POSITION = 12;
	private static final Integer THE_BRIDGE_POSITION = 6;
	private static final Integer WIN_GAME_POSITION = 63;
	private static final int[] GOOSE_POSITIONS = new int [] {5, 9, 14, 18, 23, 27};
	private Set<String> players;
	private Boolean started;
	private Boolean finished;
	private Map<String, Integer> playerPositions;
	private Map<String, Integer> playerPreviousPositions;
	Dice dice1;
	Dice dice2;
	
	public GooseGame(Dice dice1, Dice dice2) {
		this.dice1 = dice1;
		this.dice2 = dice2;
		playerPositions = new HashMap<>();
		playerPreviousPositions = new HashMap<>();
		players = new HashSet<>();
		started = false;
		finished = false;
	}
	
	public void setDices (Dice dice1, Dice dice2) {
		this.dice1 = dice1;
		this.dice2 = dice2;
	}

	public void addPlayer(String playerName) {
		if (players.contains(playerName)) {
			throw new UnsupportedOperationException(playerName + ": already existing player");
		}
		this.players.add(playerName);
		this.playerPreviousPositions.put(playerName, 0);
		this.playerPositions.put(playerName, 0);
	}
	
	public Set<String> getPlayers() {
		return this.players;
	}

	public String getPlayersNames() {
		StringBuilder playersListString = new StringBuilder();
		this.players.forEach(player -> playersListString.append(player + ", "));
		return "players: " + playersListString.substring(0, playersListString.length() - 2);
	}
	
	public Integer getPlayerPosition(String player) {
		return this.playerPositions.get(player);
	}
	
	public void setPlayerPosition(String player, Integer newPosition) {
		this.playerPreviousPositions.put(player, newPosition);
		this.playerPositions.put(player, newPosition);
	}

	public String throwPlayerDice(String player) {
		Integer dice1 = this.dice1.throwDice();
		Integer dice2 = this.dice2.throwDice();
		if (!this.started) {
			throw new UnsupportedOperationException("Game has not started!");
		}
		
		Integer playerOldPosition = this.playerPositions.get(player);
		Integer newPosition = playerOldPosition + dice1 + dice2;
		
		this.playerPreviousPositions.put(player, playerOldPosition);
		
		StringBuilder playerResponseMessage = new StringBuilder();
		
		if (newPosition == WIN_GAME_POSITION) {
			playerWinsGame(player, dice1, dice2, playerOldPosition, newPosition, playerResponseMessage);
			return playerResponseMessage.toString();
		}
		
		if (newPosition == THE_BRIDGE_POSITION) {
			newPosition = THE_BRIDGE_JUMP_POSITION;
			playerResponseMessage.append(normalMoveMessage(player, playerOldPosition.toString(), "The Bridge", dice1, dice2) + "! Pippo jumps to 12" );
		}
		
		if (isGooseSpace(newPosition)) {
			newPosition =  gooseMove(player, dice1, dice2, playerOldPosition, newPosition, playerResponseMessage);
		}
		
		if (newPosition > WIN_GAME_POSITION) {
			Integer bouncePosition = WIN_GAME_POSITION - (newPosition - WIN_GAME_POSITION);
			newPosition = bouncePosition;
			playerResponseMessage.append(bounceMoveMessage(player, dice1, dice2, playerOldPosition, bouncePosition));
		}
		
		if (playerResponseMessage.toString().isEmpty()) {
			playerResponseMessage.append(normalMoveMessage(player, playerOldPosition.toString(), newPosition.toString(), dice1, dice2));
		}
		
		final Integer playerNewPosition = newPosition;
		if (anotherPlayerInNewPosition(player, playerNewPosition)) {
			moveOtherPlayerToThisPlayerOldPosition(player, playerOldPosition, playerResponseMessage, playerNewPosition);
		}	
		
		this.playerPositions.put(player, newPosition);
		
		return playerResponseMessage.toString(); 
	}

	private boolean anotherPlayerInNewPosition(String player, final Integer playerNewPosition) {
		return this.playerPositions
				   .entrySet()
				   .stream()
				   .anyMatch( entrySet -> entrySet.getValue().equals(playerNewPosition) 
						&& !entrySet.getKey().equals(player) );
	}

	private void moveOtherPlayerToThisPlayerOldPosition(String player, Integer playerOldPosition,
			StringBuilder playerResponseMessage, final Integer playerNewPosition) {
		String existingPlayerInPosition = this.playerPositions.entrySet()
															  .stream()
															  .filter( playerPositionEntry -> playerPositionEntry.getValue().equals(playerNewPosition) 
																	  && !playerPositionEntry.getKey().equals(player) )
															  .map(entrySet -> entrySet.getKey())
															  .findFirst()
															  .get();
		playerResponseMessage.append(". On " + playerNewPosition +  " there is " + existingPlayerInPosition + ", who returns to " + playerOldPosition);
		this.playerPreviousPositions.put(existingPlayerInPosition, this.playerPositions.get(existingPlayerInPosition));
		this.playerPositions.put(existingPlayerInPosition, playerOldPosition);
	}

	private void playerWinsGame(String player, Integer dice1, Integer dice2, Integer playerOldPosition,
			Integer newPosition, StringBuilder playerResponseMessage) {
		playerResponseMessage.append(normalMoveMessage(player, playerOldPosition.toString(), newPosition.toString(), dice1, dice2) 
				+ ". "+ player + " Wins!!");
		this.playerPositions.put(player, newPosition);
		this.finished = true;
	}
	
	private Integer gooseMove(String player, Integer dice1, Integer dice2, Integer currentPosition, Integer nextPosition,
			StringBuilder playerResponseMessage) {
		playerResponseMessage.append(normalMoveMessage(player, currentPosition.toString(), nextPosition.toString() + ", The Goose. ", dice1, dice2)); 

		if (isGooseSpace(nextPosition)) {
			return gooseMultipleMove(player, dice1, dice2, currentPosition, nextPosition, playerResponseMessage);
		}
		
		return nextPosition;
	}
	
	private Integer gooseMultipleMove(String player, Integer dice1, Integer dice2, Integer currentPosition, Integer nextPosition,
			StringBuilder playerResponseMessage) {
		final Integer nextPositionAfterGooseMove = nextPosition + dice1 + dice2;
		final String gooseSpaceMessage = isGooseSpace(nextPositionAfterGooseMove) ?  ", The Goose. " : "";
		playerResponseMessage.append(player + " moves again and goes to " + nextPositionAfterGooseMove + gooseSpaceMessage);
		if (isGooseSpace(nextPositionAfterGooseMove)) {
			return gooseMultipleMove(player, dice1, dice2, currentPosition, nextPositionAfterGooseMove, playerResponseMessage);
		}
		return nextPositionAfterGooseMove;
	}
	
	private boolean isGooseSpace (Integer space) {
		if (IntStream.of(GOOSE_POSITIONS).anyMatch(goosePosition -> goosePosition == space)) {
			return true;
		}
		return false;
	}

	private String bounceMoveMessage(String player, Integer dice1, Integer dice2, Integer oldPosition,
			Integer bouncePosition) {

		return normalMoveMessage(player, oldPosition.toString(), WIN_GAME_POSITION.toString(), dice1, dice2) + ". " 
			+ player + " bounces! " + player + " returns to " + bouncePosition;
	}
	
	private String normalMoveMessage(String player, String oldPosition, String newPosition, 
			Integer dice1, Integer dice2) {
		return player + " rolls " + dice1.toString() + ", " + dice2.toString() + ". " + player + " moves from " 
				+ oldPosition + " to "+ newPosition;
	}

	public void startGame() {
		if (!this.started) {
			if (this.players.size() < MINIMUM_PLAYERS) {
				throw new UnsupportedOperationException("At least " + MINIMUM_PLAYERS + " players needed!");
			} else {
				this.started = true;
			}
		}
	}
	
	public boolean isGameStarted() {
		return this.started;
	}
	
	public boolean isGameFinished() {
		return this.finished;
	}

}
