package com.gibran.goosegame.launcher;

import java.util.Scanner;

import com.gibran.goosegame.dice.SixSideDice;
import com.gibran.goosegame.game.GooseGame;

public class LauncherMenu {
	private final String MOVE_PLAYER_OPTION = "move ";
	private final String START_OPTION = "start";
	private final String ADD_PLAYER_OPTION = "add player ";
	private final String EXIT_OPTION = "exit";
	GooseGame gooseGame; 
	Scanner userInput;

	public LauncherMenu(GooseGame gooseGame, Scanner userInput) {
		this.gooseGame = gooseGame;
		this.userInput = userInput;
	}
	
	public void launchMenu() {
		showStartGameMessage();
		
		String userInputString = "";
		while (!userInputString.equals(EXIT_OPTION) || gooseGame.isGameFinished()) {
			userInputString = userInput.nextLine();
			if (!gooseGame.isGameStarted() && userInputString.startsWith(ADD_PLAYER_OPTION)) {
				addPlayer(gooseGame, userInputString);
			} else if (!gooseGame.isGameStarted()) {
				processStartOrExitOptions(gooseGame, userInputString);
			} else {
				if (userInputString.startsWith(MOVE_PLAYER_OPTION)) {
					String playerName = userInputString.substring(5);
					if (!gooseGame.getPlayers().contains(playerName)) {
						showWrongPlayerMessage(playerName);
						continue;
					}

					System.out.println(gooseGame.throwPlayerDice(playerName));
					
					if (gooseGame.isGameFinished()) {
						finishGame();
						continue;
					}
				} else {
					processExitOrWrongOptions(userInputString);
				}
			}
		}
	}

	private void processExitOrWrongOptions(String userInputString) {
		switch (userInputString) {
		case EXIT_OPTION:
			showExitGameMessage();
			break;
		default:
			showWrongOptionMessage();
			break;
		}
	}

	private void finishGame() {
		System.out.println("Thanks for playing. Good game! Restarting game.");
		System.out.println();
		gooseGame = new GooseGame(new SixSideDice(), new SixSideDice());
		showStartGameMessage();
	}

	private void showWrongPlayerMessage(String playerName) {
		System.out.println(playerName + " is not playing this game! Try again. Available "
				+ gooseGame.getPlayersNames());
		System.out.println();
	}

	private void processStartOrExitOptions(GooseGame gooseGame, String userInputString) {
		switch (userInputString) {
		case START_OPTION:
			startGame(gooseGame);
			break;
		case EXIT_OPTION:
			showExitGameMessage();
			break;
		default:
			showWrongOptionMessage();
			break;
		}
	}

	private void showWrongOptionMessage() {
		System.out.println("Wrong option, choose again.\n");
		System.out.println();
	}

	private void showExitGameMessage() {
		System.out.println("Game over!");
	}

	private void startGame(GooseGame gooseGame) {
		if (gooseGame.getPlayers().size() >= 2) {
			gooseGame.startGame();
			System.out.println("Game started! Commands: ");
			System.out.println(MOVE_PLAYER_OPTION + "playerName -> Move the player with name playerName.");
			System.out.println("Available " + gooseGame.getPlayersNames());
			System.out.println();

		} else {
			System.out.println("At least two players needed! Add more players.");
			System.out.println();
		}
	}

	private void addPlayer(GooseGame gooseGame, String userInputString) {
		String playerName = userInputString.substring(11);
		if (gooseGame.getPlayers().contains(playerName)) {
			System.out.println("Player " + playerName + " was already added!");
		} else {
			gooseGame.addPlayer(playerName);
			System.out.println("Player " + playerName + " added.");
			if (gooseGame.getPlayers().size() >= 2) {
				System.out.println("There are " + gooseGame.getPlayers().size() + " players. The game can be started.");
				System.out.println();
			}
		}
	}
	
	private void showStartGameMessage() {
		System.out.println("Welcome to Goose Game! Commands:");
		System.out.println(ADD_PLAYER_OPTION
				+ "playerName -> Adds a player with the name playerName. At least 2 players needed to start the game.");
		System.out.println(START_OPTION + " -> Starts the game.");
		System.out.println(EXIT_OPTION + " -> Exits current game, at any time.");
		System.out.println();
	}


}
