package com.gibran.goosegame.launcher;

import java.util.Scanner;

import com.gibran.goosegame.dice.SixSideDice;
import com.gibran.goosegame.game.GooseGame;

public class GooseGameLauncher {

	public static void main(String[] args) {
		GooseGame gooseGame = new GooseGame(new SixSideDice(), new SixSideDice());
		
		Scanner userInput = new Scanner(System.in);
		
		LauncherMenu launcherMenu = new LauncherMenu(gooseGame, userInput);
		launcherMenu.launchMenu();
		
		userInput.close();
	}
	


}
