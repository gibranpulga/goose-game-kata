package com.gibran.goosegame.dice;

public interface Dice {

	Integer throwDice();
}
