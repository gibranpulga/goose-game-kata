package com.gibran.goosegame.dice;

import java.util.Random;
import java.util.stream.Collectors;

public class SixSideDice implements Dice {

	@Override
	public Integer throwDice() {
		return new Random()
				.ints(1, 1, 7)
				.mapToObj(Integer::valueOf)
				.collect(Collectors.toList())
				.get(0);
	}

}
