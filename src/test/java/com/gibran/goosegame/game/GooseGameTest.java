package com.gibran.goosegame.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.gibran.goosegame.dice.Dice;
import com.gibran.goosegame.dice.DiceMock;

public class GooseGameTest {
	@Rule public ExpectedException exception = ExpectedException.none();
	String playerPippo = "Pippo";
	String playerPluto = "Pluto";
	Dice diceResult1 = new DiceMock(1);
	Dice diceResult2= new DiceMock(2);
	Dice diceResult3 = new DiceMock(3);
	Dice diceResult4 = new DiceMock(4);
	Dice diceResult5 = new DiceMock(5);
	Dice diceResult6 = new DiceMock(6);
	

	@Test
	public void test_shouldAddOnePlayer() {
		GooseGame gooseGame = new GooseGame(diceResult1, diceResult1);
		gooseGame.addPlayer(playerPippo);
		assertTrue(gooseGame.getPlayersNames().equals ("players: " + playerPippo));
	}
	
	@Test
	public void test_shouldAddAnotherPlayer() {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult1);
		assertTrue(gooseGame.getPlayersNames().equals ("players: " + playerPippo + ", " + playerPluto));
		assertTrue(gooseGame.getPlayers().size() == 2);
	}
	
	@Test 
	public void test_shouldGiveErrorWhenAddingPlayerWithSameName() {
		exception.expect(UnsupportedOperationException.class);
	    exception.expectMessage(playerPippo + ": already existing player");
	    
		GooseGame gooseGame = new GooseGame(diceResult1, diceResult1);
		gooseGame.addPlayer(playerPippo);
		gooseGame.addPlayer(playerPippo);
	}
	
	@Test
	public void test_shouldNotMovePlayerIfThereIsNotAtLeastTwoPlayers () {
		exception.expect(UnsupportedOperationException.class);
	    exception.expectMessage("At least 2 players needed!");
	    
		GooseGame gooseGame = new GooseGame(diceResult1, diceResult1);
		gooseGame.addPlayer(playerPippo);
		gooseGame.startGame();
	}
	
	@Test
	public void test_shouldNotMovePlayerIfGameHasNotStarted () {
		exception.expect(UnsupportedOperationException.class);
	    exception.expectMessage("Game has not started!");
	    
		GooseGame gooseGame = new GooseGame(diceResult1, diceResult1);
		gooseGame.addPlayer(playerPippo);
		gooseGame.throwPlayerDice(playerPippo);
	}
	
	@Test
	public void test_shouldMovePlayerIfThereIsAtLeastTwoPlayers () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult2, diceResult2);
		assertEquals("Pippo rolls 2, 2. Pippo moves from 0 to 4", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(4));
		
		gooseGame.setDices(diceResult1, diceResult2);
		assertEquals("Pluto rolls 1, 2. Pluto moves from 0 to 3", gooseGame.throwPlayerDice(playerPluto));
		assertTrue(gooseGame.getPlayerPosition(playerPluto).equals(3));	
	}

	@Test
	public void test_shouldBouncePlayerIfExceedsSpace63 () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult3, diceResult2);		
		gooseGame.setPlayerPosition(playerPippo, 60);
		assertEquals("Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(61));
	}	
	
	@Test
	public void test_ifPlayerLandedOnSpace6TheBridgeThenJumpToSpace12 () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult1);
		gooseGame.setPlayerPosition(playerPippo, 4); 
		assertEquals("Pippo rolls 1, 1. Pippo moves from 4 to The Bridge! Pippo jumps to 12", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(12));
	}
	
	@Test
	public void test_ifPlayerLandedOnSpacesOfTheGooseThenMoveAgainSingleJump () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult1);
		gooseGame.setPlayerPosition(playerPippo, 3);
		assertEquals("Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(7));
	}
	
	@Test
	public void test_ifPlayerLandedOnSpacesOfTheGooseThenMoveAgainMultipleJump () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult2, diceResult2);
		gooseGame.setPlayerPosition(playerPippo, 10);
		assertEquals("Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. "
				+ "Pippo moves again and goes to 22", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(22));
	}
	
	@Test
	public void test_ifOnePlayerLandedOnSpaceOfAnotherPlayerThenAnotherPlayerGoesBackToOnePlayerPosition () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult1);
		gooseGame.setPlayerPosition(playerPippo, 15);
		gooseGame.setPlayerPosition(playerPluto, 17);
		assertEquals("Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(17));
		assertTrue(gooseGame.getPlayerPosition(playerPluto).equals(15));
		
		// Added this test to cover a previous bug, where if the player bounced from 63 to his previous position, 
		// the game would think that there was another player there, as the position hadn´t changed yet
		gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult5);
		gooseGame.setPlayerPosition(playerPippo, 60);
		assertEquals("Pippo rolls 1, 5. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 60", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(60));
	}
	
	@Test
	public void test_ifOnePlayerLandsExactlyOnPosition63HeWinsTheGame () {
		GooseGame gooseGame = createAndStartTwoPlayersGame(diceResult1, diceResult2);
		gooseGame.setPlayerPosition(playerPippo, 60);
		assertEquals("Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!", gooseGame.throwPlayerDice(playerPippo));
		assertTrue(gooseGame.getPlayerPosition(playerPippo).equals(63));
		assertTrue(gooseGame.isGameFinished());
	}
	
	private GooseGame createAndStartTwoPlayersGame(Dice dice1, Dice dice2) {
		GooseGame gooseGame = new GooseGame(dice1, dice2);
		gooseGame.addPlayer(playerPippo);
		gooseGame.addPlayer(playerPluto);
		gooseGame.startGame();
		return gooseGame;
	}
	
}
