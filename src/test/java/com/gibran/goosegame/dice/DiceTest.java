package com.gibran.goosegame.dice;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class DiceTest {

	
    @Test
    public void test_SixSideDiceShouldAlwaysThrowIntegerBetweenOneAndSix() {
    	Dice dice = new SixSideDice();
    	for (int i=0; i <100; i++) {
    		Integer result = dice.throwDice();
    		
    		assertTrue(result >= 1 && result <= 6);
    	}
    }
}
