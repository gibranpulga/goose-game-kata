package com.gibran.goosegame.dice;

import com.gibran.goosegame.dice.Dice;

public class DiceMock implements Dice{

	Integer result;
	
	public DiceMock(Integer result) {
		this.result = result;
	}
	
	@Override
	public Integer throwDice() {
		return this.result;
	}

}
