#This is a solution for this kata:

https://github.com/xpeppers/goose-game-kata

#Executing:

- Java 8 needs to be installed. To run with Maven, Maven also needs to be installed;
- With Maven, from the root folder, run:
mvn exec:java
- With the generated jar. It can be generated with command "mvn clean install". Also, I have put it on the root folder of the repository, for simplicity:
java -jar goose-game-kata-0.0.1-SNAPSHOT.jar

#Next Improvements:

- There was not any specification related to controlling who´s turn it is, so I didn´t add it. As an improvement I would put a validation so each player can take it´s turn the correct time.
- There was not any specification related to choosing who starts, so I didn´t add it. Ideally there should be a method to randomly choose who the starting player is.
- To run it I would add a maven profile to run by Docker or with spring boot. I didn´t for the sake of simplicity;
- Improve the launcher menu, maybe use Strategy or Command patterns;
- Add tests to the launcher menu. I didn´t focus too much on this class as it´s supposed to be a very simple UI. Ideally there should be tests for it.

